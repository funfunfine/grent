import typing
import re
from collections import OrderedDict

INT_RE = re.compile(br'^i(?P<integer>-?(0|[1-9]\d*))e')
STR_RE = re.compile(br'^(?P<length>\d+):')
BencodeReturnType = typing.Union[int, str, list, dict]

def bencode(obj: typing.Union[int, bytes, str, list, dict]) -> bytes:
    if isinstance(obj, int):
        return b'i' + bytes(str(obj), 'ascii') + b'e'
    elif isinstance(obj, (bytes, str)):
        if isinstance(obj, str):
            obj = bytes(obj, 'ascii')
        l = len(obj)
        return bytes(str(l), 'ascii') + b':' + obj
    elif isinstance(obj, list):
        return b'l' + b''.join(map(bencode, obj)) + b'e'
    elif isinstance(obj, dict):
        return b'd' + b''.join(
            bencode(key) + bencode(value) for key, value in
            sorted(obj.items(), key=lambda x: x[0])) + b'e'


def bdecode(string: bytes) -> BencodeReturnType:
    result, rest = _bdecode(string)
    return result


def _bdecode(string: bytes):
    if string.startswith(b'i'):
        match = re.match(INT_RE, string)
        _, rest_p = match.span()
        return int(match.group('integer')), string[rest_p:]
    elif re.match(STR_RE, string):
        match = re.match(STR_RE, string)
        length = int(match.group('length'))
        _, rest_p = match.span()
        return string[rest_p:rest_p + length], string[rest_p + length:]
    elif string.startswith(b'd') or string.startswith(b'l'):
        result = []
        rest = string[1:]
        while not rest.startswith(b'e'):
            element, rest = _bdecode(rest)
            result.append(element)
        rest = rest[1:]
        if string.startswith(b'l'):
            return result, rest
        else:
            d = OrderedDict()
            for i, j in zip(result[::2], result[1::2]):
                d[i] = j
            return d, rest
    return None, None


def bread(file: typing.BinaryIO)->BencodeReturnType:
    return bdecode(file.read())
