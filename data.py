import logging
import typing
from collections import deque
from time import time

try:
    from .torrent import Torrent
    from .block import Block
    from .piece import Piece
    from .save_manager import SaveManager
except ImportError:
    from torrent import Torrent
    from block import Block
    from piece import Piece
    from save_manager import SaveManager


class Data:
    """
    controls data of torrent
    decides what piece the connection should download next
    checks if piece is completed
    saves that piece if it is completed

    """
    BLOCKS_AMOUNT = 2 ** 5

    def __init__(self, torrent: Torrent, outdir='.'):
        self.outdir = outdir
        self.torrent = torrent

        self.saver = SaveManager(torrent=self.torrent, outdir=self.outdir)
        self.stage_queue = deque()

        self.left = self.torrent.length
        self.downloaded = 0
        self.all_downloaded = 0
        self.last_update_time = time()
        self.uploaded = 0

        self.ended = False

        self._init_pieces()

    def _init_pieces(self):
        self._pieces = [Piece(hash_sum, self.torrent.piece_length, index=i) for i, hash_sum in
                        enumerate(self.torrent.pieces)]

        self.downloaded_pieces: typing.List[Piece] = []
        self.downloaded_pieces_ind = set()
        self.downloading_pieces = []
        rest_l = self.torrent.length % self.torrent.piece_length
        self.current_piece_index = 0
        if not rest_l == 0:
            self._pieces[-1].length = rest_l
        self.left_pieces = self._pieces.copy()

    def update_piece(self, index, peer_id):
        self._pieces[index].containing_peers.add(peer_id)

    def update_pieces(self, bitfield, peer_id):
        update_indexes = [i for i, d in enumerate(bitfield.bin) if d == '1']
        for index in update_indexes:
            self.update_piece(index, peer_id)

    def get_piece(self, peer_id):
        self.left_pieces = sorted(self.left_pieces, key=lambda piece: len(piece.containing_peers), reverse=True)
        try:
            piece = next((piece for piece in self.left_pieces if peer_id in piece.containing_peers))
            self.left_pieces.remove(piece)
            self.downloading_pieces.append(piece)
        except StopIteration:
            piece = None
            self.stage_queue.append(piece)
            self.ended = True
        return piece

    def on_downloaded_piece(self, piece: Piece):
        self.downloading_pieces.remove(piece)
        if piece.is_complete and piece.is_hash_equal:
            logging.debug('Piece {} is okay'.format(piece))
            self.downloaded_pieces.append(piece)
            self.downloaded_pieces_ind.add(piece.index)
            self.downloaded += piece.length
            self.left -= piece.length
            self.stage_queue.append(piece.length)
            self.saver.write_piece(piece.index, piece.data)
            return True
        else:
            logging.debug('Piece {} is not okay'.format(piece))
            self.left_pieces.append(piece)
            piece.clear()
            return False

    def remove_peer(self, peer_id):
        for piece in self.left_pieces:
            piece.containing_peers.remove(peer_id)

    def set_piece_back(self, piece):
        self.left_pieces.append(piece)
        self.downloading_pieces.remove(piece)
        pass
