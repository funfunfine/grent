import logging
import shutil

try:
    from .torrent import Torrent
except ImportError:
    from torrent import Torrent
import os
import tempfile


class SaveManager:
    def __init__(self, torrent: Torrent, outdir=None):
        self.outdir = outdir
        self.torrent = torrent
        self.single = self.torrent.single

        tempfile.tempdir = outdir
        self.file = tempfile.NamedTemporaryFile(mode='wb+')
        logging.debug('Name of file is {} '.format(self.file.name))

    def write_piece(self, index, data):
        logging.debug('Saving piece {}'.format(index))
        position = index * self.torrent.piece_length
        self.file.seek(position)
        self.file.write(data)

    def finalize(self):
        logging.debug('Finalizing')
        if self.single:
            self._finalize_single()
        else:
            self._finalize_multi()
        self.file.close()

    def _finalize_multi(self):
        os.lseek(self.file, 0, os.SEEK_SET)

    def _finalize_single(self):
        result_file = self.torrent.file
        path = result_file.path
        path = os.path.join(self.outdir if self.outdir else '.', path)
        with open(path, 'wb+') as rf:
            self.file.seek(0)
            data = self.file.read()
            rf.write(data)
