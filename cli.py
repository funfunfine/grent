import asyncio
import logging

import click

from gui.gui import PauseToken

try:
    from .controller import Controller
except:
    from controller import Controller
logging.basicConfig(format='%(asctime)s\t%(filename)s\t%(funcName)s:\t\t %(message)s',
                    datefmt='%H:%M:%S')
logging.basicConfig(level=logging.CRITICAL)


@click.command()
@click.argument('torrent_file', type=click.File(mode='rb'))
@click.argument('out_dir', type=click.Path(exists=True, file_okay=False, resolve_path=True, writable=True))
@click.option('-d', '--debug', is_flag=True)
def main(torrent_file, out_dir, debug):
    if debug:
        logging.getLogger().setLevel(level=logging.DEBUG)
    logging.info('Started with file {} and output directory {}'.format(torrent_file.name, out_dir))

    c = Controller(torrent_file)
    with click.progressbar(length=len(c.torrent.pieces)) as pg_bar:
        asyncio.get_event_loop().run_until_complete(_main(c, pg_bar))


async def _main(c: Controller, pg_bar: click.progressbar):
    await c.main_loop(PauseToken(), print)


if __name__ == '__main__':
    main()
