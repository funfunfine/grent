import asyncio
import logging
try:
    from .messages import *
except ImportError:
    from messages import *
import traceback as tb


class MessageFactory(asyncio.Queue):
    def __init__(self, reader, chunk_size, buffer):
        super().__init__()

        self.chunk_size = chunk_size
        self.reader = reader
        self.buffer = buffer
        self.code_type_switch = {
            0: Choke,
            1: Unchoke,
            2: Interested,
            3: NotInterested,
            4: Have,
            5: Bitfield,
            6: Request,
            7: Piece
        }

    def run(self):
        self._factory_coroutine = asyncio.ensure_future(self._factory())

    def stop(self):
        self._factory_coroutine.cancel()

    async def _factory(self):
        while True:
            try:
                data = await self.reader.read(self.chunk_size)
                if len(data) == 0:
                    await self.put(None)
                    return
                self.buffer += data
                message = self.parse_buffer()
                if message:
                    await self.put(message)
            except Exception as e:
                logging.error('Error happened while receiving messages: {}'.format(e))
                await self.put(None)
                return

    def parse_buffer(self):
        try:
            length, = unpack('>I', self.buffer[:4])
            message = self.buffer[:4 + length]
            if len(message) - 4 < length:
                return None
            self.buffer = self.buffer[4 + length:]
            if length == 0:
                logging.debug('Got KeepAlive message')
                return KeepAlive()
            code, = unpack('>b', message[4:5])
            if code in self.code_type_switch:
                return self.code_type_switch[code].decode(message)
            return None
        except Exception as e:
            print('Error while parsing buffer'.format(tb.format_exc()))
