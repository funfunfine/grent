import logging as log
from abc import abstractmethod
from struct import pack, unpack

from bitstring import BitArray


class MessageFormatException(Exception):
    pass


class Message:

    KeepAlive = -1
    Choke = 0
    Unchoke = 1
    Interested = 2
    NotInterested = 3
    Have = 4
    Bitfield = 5
    Request = 6
    Piece = 7
    ClosedSocket = 100
    @abstractmethod
    def encode(self):
        pass

    @classmethod
    @abstractmethod
    def decode(cls, message):
        pass


class KeepAlive(Message):
    def __init__(self):
        pass

    def encode(self):
        return pack('>I', 0)

    @classmethod
    def decode(cls, message):
        return cls()


class Choke(Message):
    def __init__(self):
        self.length = 1
        self.code = Message.Choke

    def encode(self):
        return pack('>Ib', self.length, self.code)

    @classmethod
    def decode(cls, message):
        return cls()


class Unchoke(Message):
    def __init__(self):
        self.length = 1
        self.code = Message.Unchoke

    def encode(self):
        return pack('>Ib', self.length, self.code)

    @classmethod
    def decode(cls, message):
        return cls()

class Interested(Message):
    def __init__(self):
        self.length = 1
        self.code = Message.Interested

    def encode(self):
        return pack('>Ib', self.length, self.code)

    @classmethod
    def decode(cls, message):
        return cls()


class NotInterested(Message):
    def __init__(self):
        self.length = 1
        self.code = Message.NotInterested

    def encode(self):
        return pack('>Ib', self.length, self.code)

    @classmethod
    def decode(cls, message):
        return cls()


class Have(Message):
    m_format = '>IbI'

    def __init__(self, index):
        self.code = Message.Have
        self.index = index
        self.length = 1 + 4

    def encode(self):
        return pack(Have.m_format, self.length, self.code, self.index)

    @classmethod
    def decode(cls, message):
        length, code, index = unpack(cls.m_format, message)
        return cls(index)


class Bitfield(Message):
    def __init__(self, bitfield):
        self.code = Message.Bitfield
        self.bitfield = BitArray(bytes=bitfield)
        self.length = 1 + len(self.bitfield)

    def encode(self):
        m_format = '>Ib{}s'.format(str(self.length - 1))
        return pack(m_format, self.length, self.code, self.bitfield)

    @classmethod
    def decode(cls, message):
        m_format = '>Ib{}s'.format(len(message) - 5)
        length, code, bitfield = unpack(m_format, message)
        return cls(bitfield)


class Request(Message):
    m_format = '>IbIII'

    def __init__(self, index, begin, block_length):
        self.block_length = block_length
        self.begin = begin
        self.index = index
        self.length = 1 + 4 + 4 + 4
        self.code = Message.Request

    def encode(self):
        return pack(Request.m_format, self.length, self.code, self.index, self.begin, self.block_length)

    def __str__(self):
        return 'Request message: {}, {}, {}'.format(self.index, self.begin, self.block_length)

    @classmethod
    def decode(cls, message):
        _, __, index, begin, length = unpack(Request.m_format, message)
        return cls(index, begin, length)


class Piece(Message):
    def __init__(self, index, begin, block):
        self.begin = begin
        self.index = index
        self.block = block
        self.code = Message.Piece
        self.length = 1 + 4 + 4 + len(block)

    def encode(self):
        m_format = '>IbII{}s'.format(len(self.block))
        return pack(m_format, self.length, self.code, self.index, self.begin, self.block)

    def __str__(self):
        return "Piece at {} {} of length {}".format(self.index, self.block[:20], self.length)

    @classmethod
    def decode(cls, message):
        m_format = '>IbII{}s'.format(len(message) - 9 - 4)
        length, code, index, begin, block = unpack(m_format, message)
        return cls(index, begin, block)


class Handshake(Message):
    m_format = '>B19s8x20s20s'

    def __init__(self, info_hash, peer_id):
        self.info_hash = info_hash
        self.peer_id = peer_id

    def encode(self):
        if not isinstance(self.peer_id,bytes):
            self.peer_id = self.peer_id.encode('utf-8')
        return pack(Handshake.m_format, 19, b'BitTorrent protocol', self.info_hash, self.peer_id)

    @classmethod
    def decode(cls, message):
        try:
            pstrlen, pstr, reserved, info_hash, peer_id = unpack(cls.m_format, message)
        except:
            pstrlen, pstr, info_hash, peer_id = unpack(cls.m_format, message)
        return cls(info_hash, peer_id)

    def __str__(self):
        return str(self.encode())
