import logging
from hashlib import sha1

try:
    from block import Block
except:
    from block import Block

class Piece:
    def __init__(self, hash_sum, piece_length, index=0, blocks_amount=2**5):
        self.hash_sum = hash_sum
        self.length = piece_length
        self.index = index

        self.containing_peers = set()

        self.status = Block.Missing

        self.block_length = self.length // blocks_amount

        self._blocks = [Block(self.block_length, index=i) for i in range(blocks_amount)]

        r = self.length % blocks_amount
        if r != 0:
            self._blocks.append(Block(r, index=blocks_amount))

        self.blocks = (b for b in self._blocks)

    @property
    def data(self):
        return b''.join(block.data for block in self._blocks)

    def __str__(self):
        return 'Piece at index {} with length {}'.format(self.index, self.length)

    def add_block(self, begin, data):

        index = begin // self.block_length
        logging.debug('Adding block to {} at {}'.format(self.__str__(), index))
        b = self._blocks[index]
        self._blocks[index].data = data
        self._blocks[index].status = Block.Exists

    @property
    def is_complete(self):
        try:
            return all(b.status == Block.Exists for b in self._blocks)
        except Exception as e:
            logging.error('Is comlete {}'.format(e))

    def clear(self):
        for block in self.blocks:
            block.status = Block.Missing
            block.data = b''
    @property
    def is_hash_equal(self):
        message = b''.join(block.data for block in self._blocks)
        hash = sha1(message).digest()
        return self.hash_sum == hash