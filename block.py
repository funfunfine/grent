class Block:
    Missing = -1
    Downloading = 0
    Exists = 1

    def __init__(self, length, index=0):
        self.length = length
        self.index = index

        self.status = Block.Missing
        self.data = b''

    def __str__(self):
        return 'Block at index {} with length {}'.format(self.index, self.length)