import logging
import os
from collections import namedtuple
from hashlib import sha1

from bencoding import bencode, bread


class TorrentFileFormatError(Exception):
    pass


class Torrent:
    """
    Torrent file processor

    """

    def __init__(self, torrent_file):
        self.is_udp = None
        logging.debug('Started processing Torrent file...')

        self.metainfo = bread(torrent_file)
        self._check_metainfo()

        self.info = self.metainfo.get(b'info')
        self._check_info()
        logging.debug('Torrent file is okay')
        self.announce_url = str(self.metainfo.get(b'announce'), 'utf-8')
        self.info_hash = sha1(bencode(self.info)).digest()
        self.raw_pieces = self.info[b'pieces']
        length = len(self.raw_pieces)
        self.pieces = [self.raw_pieces[i:i + 20] for i in range(0, length, 20)]
        self.piece_length = self.info.get(b'piece length')

        if b'files' in self.info:
            self._multiple_files()
        elif b'length' in self.info:
            self._single_file()

    def _check_metainfo(self):
        must_have_keys = [b'announce', b'info']
        if not all(key in self.metainfo for key in must_have_keys):
            logging.error('Torrent file is invalid')
            raise TorrentFileFormatError()

    def _check_info(self):
        must_have_keys = [b'piece length', b'pieces']
        optional_keys = [b'files', b'length']
        if not all(key in self.info for key in must_have_keys) or not any(key in self.info for key in optional_keys):
            logging.error('Torrent file is invalid')
            raise TorrentFileFormatError()

    def _multiple_files(self):
        logging.debug('Got a multiple-file Torrent')
        self.single = False
        self.dir_name = self.info.get(b'name')
        self.file_list = self.info.get(b'files')
        self._process_files()

    def _process_files(self):
        # probably it is better to make a comprehension out of this method
        self.files = []
        for file_dict in self.file_list:
            k_length, k_path = file_dict
            length = file_dict[k_length]
            path = list(map(lambda s: s.decode('utf-8'), file_dict[k_path]))
            self.files.append(File(length, path))
        self.length = sum((f.length for f in self.files))

    def _single_file(self):
        logging.debug('Got a single-file Torrent')
        self.single = True
        length = self.info.get(b'length')
        self.length = length
        filename = str(self.info.get(b'name'), 'utf-8')
        self.file = File(length, filename)

    def __str__(self):
        return 'Torrent object with {}, in {} file mode'.format(self.announce_url,
                                                                'single' if self.single else 'multiple')


File = namedtuple('File', ['length', 'path'])
