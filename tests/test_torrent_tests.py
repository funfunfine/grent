import os
import unittest

from ..torrent import Torrent


class TorrentTest(unittest.TestCase):
    def test_announce(self):
        test_path = os.path.join('..', 'ubuntu.torrent')
        with open(test_path, 'rb') as tf:
            torrent = Torrent(tf)
        self.assertEqual(torrent.announce_url, r'http://torrent.ubuntu.com:6969/announce')
        self.assertEqual(torrent.piece_length, 524288)

    def test_single_file(self):
        test_path = os.path.join('..', 'ubuntu.torrent')
        with open(test_path, 'rb') as tf:
            torrent = Torrent(tf)
        self.assertEqual(torrent.single, True)
        self.assertEqual(torrent.file.length, 1953349632)
        self.assertEqual(torrent.file.path, '.\\ubuntu-18.04.1-desktop-amd64.iso')

    def test_multiple_file(self):
        test_path = os.path.join('..', 'pics.torrent')
        with open(test_path, 'rb') as tf:
            torrent = Torrent(tf)
        self.assertEqual(torrent.single, False)
        self.assertEqual(torrent.files[0].path,'1600х1200\\Win7_10.jpg')


if __name__ == '__main__':
    unittest.main()
