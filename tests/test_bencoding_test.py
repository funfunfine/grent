import unittest

try:
    from ..bencoding import bdecode, bencode
except ImportError:
    from tests.bencoding import bdecode, bencode


class test_bencoding(unittest.TestCase):
    def test_simple_objects_encoding(self):
        self.assertEqual(bencode(3), b'i3e')
        self.assertEqual(bencode(-2), b'i-2e')
        self.assertEqual(bencode('string'), b'6:string')
        self.assertEqual(bencode(b'string'), b'6:string')
        self.assertEqual(bencode(''), b'0:')
        self.assertEqual(bencode([]), b'le')
        self.assertEqual(bencode([1, 2]), b'li1ei2ee')
        self.assertEqual(bencode(dict()), b'de')
        self.assertEqual(bencode({b'spam': [b'a', b'b']}), b'd4:spaml1:a1:bee')
        self.assertEqual(bencode({b'spam': [b'a', b'b']}), b'd4:spaml1:a1:bee')

    def test_simple_objects_decoding(self):
        self.assertEqual(3, bdecode(b'i3e'))
        self.assertEqual(-2, bdecode(b'i-2e'))
        self.assertEqual(b'string', bdecode(b'6:string'))
        self.assertEqual([], bdecode(b'le'))
        self.assertEqual([1, 2], bdecode(b'li1ei2ee'))
        self.assertEqual({}, bdecode(b'de'))
        self.assertEqual({b'spam': [b'a', b'b']}, bdecode(b'd4:spaml1:a1:bee'))


if __name__ == '__main__':
    unittest.main()
