import asyncio
import logging
import traceback
import typing
from time import time

from block import Block
from data import Data
from message_factory import MessageFactory
from messages import *
from peers import RemotePeer, Client

import traceback as tb

class Connection:
    """
    Closes when:
        remote peer closes the socket
        when all the pieces is downloaded
        // when there is no unchoke for a long time
    """
    CHUNK_SIZE = 2 ** 10
    READY = 0
    ONGOING = 1
    CLOSED = 2
    DROP_TIME = 2

    def __init__(self, client: Client, remote_peer: RemotePeer, data: Data, speed_callback: print):
        self.remote_peer = remote_peer
        self.client = client
        self.data = data
        self.speed_callback = speed_callback
        self.choked = True
        self.interested = False
        self.connection_status = Connection.READY

        self._init_switches()

        self.requesting_piece = None
        self.change_piece = None

    def _init_switches(self):
        self.message_switch = {
            Bitfield: self.process_bitfield,
            Have: self.process_have,
            KeepAlive: self.process_keep_alive,
            Choke: self.process_choke,
            Unchoke: self.process_unchoke,
            Interested: self.process_interested,
            NotInterested: self.process_not_interested,
            Request: self.process_request,
            Piece: self.process_piece_block
        }

    async def is_available(self):
        return await self.try_connect() and await self.make_handshake()

    async def make_handshake(self):
        await self.send_handshake()
        rest = await self.receive_handshake()
        if rest is None:
            return False
        self.message_factory = MessageFactory(self.reader, Connection.CHUNK_SIZE, rest)
        return True

    async def send_handshake(self):
        handshake = Handshake(self.client.torrent.info_hash, self.client.peer_id).encode()
        logging.debug('Sending handshake to {}:{}...'.format(self.remote_peer.ip, self.remote_peer.port))
        self.writer.write(handshake)
        await self.writer.drain()

    async def receive_handshake(self):
        hs_len = 19 + 49
        buffer = b''
        while len(buffer) < hs_len:
            data = await self.reader.read(Connection.CHUNK_SIZE)
            if len(data) == 0:
                logging.debug('Remote peer closed the socket')
                return None
            buffer += data
        handshake = Handshake.decode(buffer[:hs_len])
        if not handshake.info_hash == self.client.torrent.info_hash:
            logging.error('Wrong info_hash of peer {}'.format(self.remote_peer))
            return None
        self.remote_peer.peer_id = handshake.peer_id
        return buffer[hs_len:]

    async def try_connect(self):
        logging.debug('Trying to connect to {}:{}'.format(
            self.remote_peer.ip,
            self.remote_peer.port))
        try:
            self.reader, self.writer = await asyncio.wait_for(
                asyncio.open_connection(self.remote_peer.ip, self.remote_peer.port),
                Connection.DROP_TIME
            )
            if self.reader and self.writer:
                logging.debug('Successfully connected to {}:{}'.format(self.remote_peer.ip, self.remote_peer.port))
                return True
        except TimeoutError:
            logging.error('Connection timeout')
        except Exception as e:
            logging.error('Connection failed: {}'.format(e))

    def start(self, token):
        self.main_loop_future = asyncio.ensure_future(self.main_loop(token))
        self.message_factory.run()

    def stop(self):
        self.message_factory.stop()
        self.main_loop_future.cancel()

    async def main_loop(self, token):
        while True:
            try:
                if token.deleted is True:
                    return
                if token.paused is True:
                    logging.debug("paused")
                    await asyncio.sleep(5)
                    continue
                if not self.interested or self.choked:
                    await self.send_interested()

                message = await self.message_factory.get()
                if message is None:
                    logging.error('Peer closed the socket or the error inside happened')
                    return self.dispose()

                logging.debug('Got {} message'.format(type(message).__name__))
                self.message_switch[type(message)](message)

                if not self.choked and self.interested and self.requesting_piece is None:
                    self.requesting_piece = self.get_next_piece()
                if not self.choked and self.interested and self.requesting_piece:
                    mark = await self.send_request()
                    if mark == -1:
                        return self.dispose()
            except Exception as e:
                logging.error('Got error in connection: {}'.format(e))

    def process_bitfield(self, message):
        self.data.update_pieces(message.bitfield, self.remote_peer.peer_id)

    def process_have(self, message):
        self.data.update_piece(message.index, self.remote_peer.peer_id)

    def process_keep_alive(self, message):
        pass

    def process_choke(self, _):
        self.choked = True

    def process_unchoke(self, _):
        self.choked = False

    def process_interested(self, _):
        self.remote_peer.interested = True

    def process_not_interested(self, _):
        self.remote_peer.interested = False

    def process_request(self, message):
        logging.debug('got request {}'.format(message))
        req = Request.decode(message)
        if req.index in self.data.downloaded_pieces_ind:
            data = self.data.downloaded_pieces[req.index].data[req.begin:req.begin + req.length]
            message = Piece(req.index, req.begin, data).encode()
            self.writer.write(message.encode())
            logging.debug("Sent Piece {}".format(message))
            asyncio.ensure_future(self.writer.drain())

    def process_piece_block(self, message):
        piece_index, begin, data = message.index, message.begin, message.block
        logging.debug('Got block {}'.format(piece_index))

        self.data.all_downloaded += len(data)
        dt = time() - self.data.last_update_time
        self.speed_callback(len(data) / dt, self.data.all_downloaded)
        self.data.last_update_time = time()

        if self.change_piece:
            self.change_piece.add_block(begin, data)
            asyncio.ensure_future(self.on_downloaded_piece(self.change_piece))
            self.change_piece = None
        else:
            self.requesting_piece.add_block(begin, data)

    def dispose(self):
        if self.requesting_piece:
            self.requesting_piece.clear()
            self.data.remove_peer(self.remote_peer.peer_id)
            self.data.set_piece_back(self.requesting_piece)
        self.connection_status = Connection.CLOSED
        self.stop()

    async def send_interested(self):
        logging.debug('Sent interested')
        self.interested = True
        message = Interested()
        self.writer.write(message.encode())
        await self.writer.drain()

    def get_next_piece(self) -> typing.Union[Piece, None]:
        piece = self.data.get_piece(self.remote_peer.peer_id)
        if piece is None:
            self.ended = True
            self.data.saver.finalize()
            self.dispose()
        logging.debug('Chose piece: {}'.format(piece))
        return piece

    def get_next_block(self) -> typing.Union[Block, None]:
        try:
            return next(self.requesting_piece.blocks)
        except StopIteration:
            logging.debug('Run out of blocks for this piece')
            self.change_piece = self.requesting_piece
            self.requesting_piece = self.get_next_piece()
            return next(self.requesting_piece.blocks)

    async def send_request(self):
        if self.requesting_piece is None:
            return -1
        block = self.get_next_block()
        block.status = Block.Downloading
        offset = block.index * self.requesting_piece.block_length
        req = Request(self.requesting_piece.index, offset, block.length)
        self.writer.write(req.encode())
        await self.writer.drain()

    async def on_downloaded_piece(self, piece):
        if self.data.on_downloaded_piece(piece):
            await self.send_have(piece)

    async def send_have(self, piece):
        index = piece.index
        logging.debug('Sending Have({})'.format(index))
        have = Have(index)
        self.writer.write(have.encode())
        await self.writer.drain()
