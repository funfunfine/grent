import asyncio
import logging
import threading
import typing
from functools import partial
from itertools import cycle
from time import time
from typing import Mapping

from controller import Controller
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import *
from kivy.uix.modalview import ModalView
from kivymd.filemanager import MDFileManager
from kivymd.navigationdrawer import NavigationLayout
from kivymd.tabs import MDTab
from kivymd.theming import ThemeManager
from kivymd.toast import toast

PAUSED = 'paused'
ACTIVE = 'active'
STARTING = 'starting'
DOWNLOADING = 'downloading'
DOWNLOADED = 'downloaded'
CONNECTING = 'connecting to peers'

logging.basicConfig(format='%(asctime)s\t%(filename)s\t%(funcName)s:\t\t %(message)s',
                    datefmt='%H:%M:%S')
logging.basicConfig(level=logging.DEBUG)

VERSION = "0.1"
AUTHOR = "funfunfine"

ROOT = logging.getLogger()
ROOT.setLevel(logging.DEBUG)


class TorrentTab(MDTab):
    progress_value = NumericProperty(0)
    torrent_name = StringProperty('template.torrent')
    torrent_size = NumericProperty(0)
    speed = NumericProperty(0)
    downloaded = NumericProperty(0)
    time_left = NumericProperty(0)
    state = OptionProperty(ACTIVE, options=[ACTIVE, PAUSED])
    download_state = OptionProperty(STARTING, options=[STARTING, DOWNLOADING, DOWNLOADED, PAUSED,
                                                       CONNECTING])


class LogTab(MDTab):
    log_text = StringProperty('')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.buffer = []

    def log(self, logs):
        self.log_text += logs

    def write(self, text):
        self.buffer.append(text)

    def flush(self):
        self.log(''.join(self.buffer))
        self.buffer = []


class GrentLayout(NavigationLayout):
    pass


class GrentInterface(App):
    theme_cls = ThemeManager()
    themes = cycle(
        ['Pink', 'Blue', 'Indigo', 'BlueGrey', 'Brown', 'LightBlue', 'Purple', 'Grey', 'Yellow', 'LightGreen',
         'DeepOrange', 'Green', 'Red', 'Teal', 'Orange', 'Cyan', 'Amber', 'DeepPurple', 'Lime'])
    theme_cls.primary_palette = 'Blue'
    title = "Grent Torrent client"
    version = VERSION
    main_widget = None
    tabs = ListProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.manager_open = False
        self.manager = None
        self.tab_map: typing.Dict[str, TorrentTab] = dict()

        self.last_speed_update = time()

    def on_start(self):
        ROOT.addHandler(logging.StreamHandler(self.main_widget.ids.logs))
        self.main_widget.ids.tabs.current = 'logs'
        self.loop = asyncio.get_event_loop()
        asyncio.ensure_future(self.stub())
        self.loop_thread = threading.Thread(target=self.loop.run_forever)
        self.loop_thread.start()

    async def stub(self):
        while True:
            await asyncio.sleep(5)

    def delete_torrent(self, torrent_name):
        pass

    def change_theme(self):
        self.theme_cls.primary_palette = next(self.themes)

    def pause_torrent(self, torrent_name):
        tab = self.tab_map[torrent_name]
        tab.state = PAUSED if tab.state == ACTIVE else ACTIVE

    def close(self):
        logging.debug('Close button was pressed')
        self.prepare_for_closing()
        self.stop()

    def add_first_torrent(self, *args):
        logging.debug('Add first torrent button was pressed')
        self.file_manager_open()

    def select_path(self, path):
        logging.debug('Got selected path: {}'.format(path))
        self.exit_manager()
        name, size = (self.start_download(path))
        if name is not None:
            result = self.create_tab(name, size)
            if result is False:
                toast("This one is already there")

    def exit_manager(self, *args):
        self.manager.dismiss()
        self.manager_open = False

    def on_stop(self):
        self.prepare_for_closing()
        logging.debug('closing...')

    def file_manager_open(self):
        logging.debug('Opening file manager')
        if not self.manager:
            self.manager = ModalView(size_hint=(1, 1), auto_dismiss=False)
            self.file_manager = MDFileManager(
                exit_manager=self.exit_manager, select_path=self.select_path)
            self.manager.add_widget(self.file_manager)
            self.file_manager.show('../..')
        self.manager_open = True
        self.manager.open()

    def show_info(*args):
        toast('Grent torrent client by {}, version {}'.format(AUTHOR, VERSION))

    def build(self):
        self.main_widget = Builder.load_file('gui_template.kv')
        return self.main_widget

    def create_tab(self, name, size):
        tab = TorrentTab()
        tab.torrent_size = size
        tab.torrent_name = name
        if name in self.tab_map:
            return False
        tabs = self.main_widget.ids.tabs
        tabs.add_widget(tab)
        self.tab_map[name] = tab
        tab.download_state = STARTING
        tabs.current = tab.name

    def start_download(self, path):
        return None, None

    def prepare_for_closing(self):
        pass


class PauseToken:
    def __init__(self):
        self.paused = False
        self.deleted = False

    def switch(self):
        self.paused = not self.paused

    def delete(self):
        self.deleted = True


class Grent(GrentInterface):
    def __init__(self, **kwargs):
        self.token_map = dict()
        self.controllers: Mapping[str, Controller] = dict()
        self.future_map = dict()
        super().__init__(**kwargs)

    def update_speed(self, torrent_name, speed, downloaded):
        if time() < self.last_speed_update + 2:
            return
        self.last_speed_update = time()
        tab = self.tab_map[torrent_name]
        if tab.download_state == STARTING or tab.download_state == PAUSED:
            tab.download_state = DOWNLOADING
        tab.speed = speed // 1024
        tab.downloaded = downloaded // 1024
        seconds = self.controllers[torrent_name].data.left // 1024 // tab.speed
        tab.time_left = seconds / 60

    def end_tab(self, torrent_name):
        tab = self.tab_map[torrent_name]
        tab.speed = 0
        tab.progress_value = 100
        controller = self.controllers[torrent_name]
        controller.close()
        tab.download_state = DOWNLOADED
        self.future_map[torrent_name].cancel()
        del self.future_map[torrent_name]

    def update_progress_bar(self, torrent_name, value):
        print(torrent_name, value)
        tab = self.tab_map[torrent_name]
        if value is None:
            toast("{} has been downloaded".format(torrent_name))
            self.end_tab(torrent_name)
            return
        tab.progress_value += value

    def pause_torrent(self, torrent_name):
        super().pause_torrent(torrent_name)
        tab = self.tab_map[torrent_name]
        tab.download_state = PAUSED
        token = self.token_map[torrent_name]
        if token.paused is False:
            tab.speed = 0
            tab.download_state = PAUSED
            toast("Paused")
        else:
            self.last_speed_update = time()
        logging.debug('Pause button was pressed for {}'.format(torrent_name))
        token.switch()

    def delete_torrent(self, torrent_name):
        logging.debug('Delete button was pressed for {}'.format(torrent_name))
        self.token_map[torrent_name].delete()
        del self.token_map[torrent_name]
        self.future_map[torrent_name].cancel()
        del self.future_map[torrent_name]
        data = self.controllers[torrent_name].data
        if data.ended is True:
            toast("File is downloaded")
        tabs = self.main_widget.ids.tabs
        tab = self.tab_map[torrent_name]
        tabs.remove_widget(tab)
        del self.tab_map[torrent_name]
        tabs.current = 'logs'

    def prepare_for_closing(self):
        self.loop.stop()

    def start_download(self, path):
        logging.debug('Starting downloading from {}'.format(path))
        with open(path, 'rb') as f:
            controller = Controller(f)
        name = path.split('/')[-1]
        logging.debug('Up to start actual download')
        token = PauseToken()
        self.token_map[name] = token
        fut = asyncio.ensure_future(
            controller.main_loop(token, partial(self.update_progress_bar, name), partial(self.update_speed, name)))
        self.future_map[name] = fut
        self.controllers[name] = controller
        return name, controller.torrent.length // 1024


if __name__ == '__main__':
    Grent().run()
