import asyncio
import logging
import sys
import traceback
from collections import deque
from time import time

import requests

try:
    from .connection_ import Connection
    from .data import Data
    from .peers import Client, RemotePeer
    from .torrent import Torrent
    from .tracker_response import TrackerResponse
except ImportError:
    from connection_ import Connection
    from data import Data
    from peers import Client, RemotePeer
    from torrent import Torrent
    from tracker_response import TrackerResponse


class Controller:
    """
    Joins everything together
    controls the flow of downloading
    gets new peers by making announce request
    creates connections to those peers
    """
    UPDATE_TIME = 15 * 60

    def __init__(self, file, outdir='.', max_connections=1):
        self.connecting = False
        self.torrent = Torrent(file)
        self.max_connections = max_connections

        self.client = Client(torrent=self.torrent)
        self.data = Data(torrent=self.torrent, outdir=outdir)

        self.event = 'started'
        self.last_announce_time = 0
        self.peers = deque()
        self.active_connections = list()

    async def main_loop(self, pause_token, progress_bar_callback, speed_callback):
        print('THUS IT HAS BEGUN')
        self.token = pause_token
        speed_measure_time = 5
        last_speed_measure_time = time()
        last_downloaded_amount = 0
        while True:
            try:
                if self.token.deleted is True:
                    break
                if self.token.paused is True:
                    logging.debug("paused")
                    await asyncio.sleep(5)
                    continue
                logging.debug('Checking time')
                if time() > self.last_announce_time + Controller.UPDATE_TIME or (
                        len(self.peers) == 0 and len(self.active_connections) == 0):
                    self.update_peer_queue()
                logging.debug('Checking peers amount: {}'.format(len(self.active_connections)))

                if self.data.ended:
                    logging.debug('Downloaded all!')
                    for c in self.active_connections:
                        c.stop()
                    progress_bar_callback(None)
                    break
                if len(self.active_connections) < self.max_connections and len(self.peers) > 0:
                    peer = self.peers.popleft()
                    connection = Connection(client=self.client,
                                            remote_peer=RemotePeer(torrent=Torrent, ip_port=peer),
                                            data=self.data, speed_callback=speed_callback)
                    if await connection.is_available():
                        logging.debug('Created connection to {}'.format(peer))
                        self.active_connections.append(connection)
                        connection.start(self.token)

                if any(connection.connection_status == Connection.CLOSED for connection in self.active_connections):
                    self.active_connections = [connection for connection in self.active_connections if
                                               connection.connection_status != Connection.CLOSED]

                while len(self.data.stage_queue) > 0:
                    stage = self.data.stage_queue.popleft()

                    print(stage, 'smth')
                    percent = stage * 100 / self.torrent.length
                    progress_bar_callback(percent)

                    if self.data.ended or stage is None:
                        self.close()
                        progress_bar_callback(None)
                        break

                await asyncio.sleep(speed_measure_time)

            except Exception as e:
                logging.error("Exception in user code:")
                logging.error("-" * 60)
                traceback.print_exc(file=sys.stderr)
                logging.error("-" * 60)

    def update_peer_queue(self):
        peers = self.make_announce()
        peers = set(peers)
        logging.debug('Peers are {}'.format(peers))
        self.last_announce_time = time()
        self.event = None if self.event == 'started' or self.event == 'stopped' else self.event
        for peer in peers:
            self.peers.append(peer)

    def make_announce(self):
        if self.torrent.is_udp:
            return self._make_udp_announce()
        else:
            return self._make_http_announce()

    def _make_udp_announce(self):
        pass

    def _make_http_announce(self):
        announce_url = self.torrent.announce_url
        payload = self._get_payload()
        response_raw = requests.get(announce_url, payload)
        logging.debug('Sent announce to {}'.format(response_raw.url))
        response = TrackerResponse(response_raw)
        logging.debug('Peers are {}'.format(response.peers))
        return response.peers

    def _get_payload(self):
        result = {'info_hash': self.torrent.info_hash,
                  'peer_id': self.client.peer_id,
                  'port': str(self.client.port),
                  'uploaded': str(self.data.uploaded),
                  'downloaded': str(self.data.downloaded),
                  'left': str(self.data.left),
                  'compact': '1'}
        if self.event:
            result['event'] = self.event
        return result

    def close(self):
        pass
