import os
import unittest
from peers import Client
from torrent import Torrent


class ClientTest(unittest.TestCase):
    def test_creation(self):
        test_path = 'ubuntu.torrent'
        with open(test_path, 'rb') as tf:
            torrent = Torrent(tf)
        client = Client(torrent=torrent)
        self.assertEqual(len(client.peer_id), 20)


if __name__ == '__main__':
    unittest.main()
