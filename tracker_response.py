import logging
import typing
from struct import unpack
try:
    from .bencoding import bdecode
except ImportError:
    from bencoding import bdecode

class TrackerResponseError(Exception):
    pass


class TrackerResponse:
    def __init__(self, response):
        self.response = response
        self.parsed_response = bdecode(self.response.content)
        logging.debug('Successfully parsed response')

        self._check_response()
        self.peers: typing.Union[bytes, typing.List[typing.Dict]] = self.parsed_response.get(b'peers')

        if isinstance(self.peers, bytes):
            logging.debug('Peers are in binary mode')
            self._process_binary_peers()
        elif isinstance(self.peers, list):
            logging.debug('Peers are in dictionary mode')
            self._process_dictionary_peers()

    def _process_dictionary_peers(self):
        self.peers = [(d[b'ip'], d[b'port']) for d in self.peers]

    def _process_binary_peers(self):
        raw_peers = self.peers
        self.peers = [('.'.join(str(k) for k in raw_peers[i:i + 4]), unpack('>H', raw_peers[i + 4:i + 6])[0]) for i in
                      range(0, len(raw_peers), 6)]
        self.peers = [(ip, port) for ip, port in self.peers if port != 0]

    def _check_response(self):
        if self.response.status_code != 200:
            raise TrackerResponseError()
        if b'failure' in self.parsed_response:
            logging.error('Something wrong with tracker or announce: {}'.format(self.parsed_response.get(b'failure')))
            return
        if b'failure reason' in self.parsed_response:
            logging.error(
                'Something wrong with tracker or announce: {}'.format(self.parsed_response.get(b'failure reason')))
            raise TrackerResponseError()
        if b'peers' not in self.parsed_response:
            logging.error('No peers and failure in tracker`s response')
            raise TrackerResponseError()
