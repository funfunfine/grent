from setuptools import setup, find_packages

setup(name='grent',
      version='0.0.1',
      packages=find_packages(),
      entry_points={'console_scripts': 'grent=grent.cli:main'}, install_requires=['BitTorrent-bencode', 'click'])
