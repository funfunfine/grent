from random import sample
from string import ascii_letters, digits




class Peer:
    def __init__(self, peer_id, torrent):
        self.peer_id = peer_id
        self.torrent = torrent


class Client(Peer):
    @classmethod
    def create_peer_id(cls):
        return '-TN' + ''.join(sample(population=set(ascii_letters + digits), k=17))

    def __init__(self, peer_id=None, torrent=None):
        super().__init__(peer_id=peer_id, torrent=torrent)
        self.port = 6881
        if peer_id is None:
            self.peer_id = Client.create_peer_id()


class RemotePeer(Peer):
    Closed = -1
    Connected = 0

    def __init__(self, torrent=None, ip_port=None):
        super().__init__(None, torrent)
        self.ip, self.port = ip_port
        self.status = RemotePeer.Closed
        self.interested = False
        self.choked = True
